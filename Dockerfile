FROM debian:9
RUN apt-get update && apt-get install -y libapache2-mod-php7.0 php7.0-cli php7.0-mysql php7.0-mysqli php7.0-gd php7.0-mcrypt php7.0-json php7.0-ldap php-pear snmp fping mariadb-client python-mysqldb rrdtool subversion whois mtr-tiny ipmitool graphviz imagemagick apache2 wget nano smokeping rsyslog
RUN mkdir -p /opt/observium
WORKDIR /opt
RUN wget http://www.observium.org/observium-community-latest.tar.gz && tar zxvf observium-community-latest.tar.gz && rm observium-community-latest.tar.gz
WORKDIR /opt/observium
COPY ./entrypoint.sh /entrypoint.sh
CMD ["/bin/bash","/entrypoint.sh","run"]
