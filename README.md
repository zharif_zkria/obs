### What is this ? ###

* This is containerized version of the Observium CE (LDAP,rsyslog,smokeping included and configured)
* Version 1.0

### How do I get set up? ###

* use the docker-compose.yml example file. `docker-compose up`.
* by default the webserver serves HTTPS only on port 443.
* by default syslog server listen on port 514 udp.
* refer to the enviroment vars table to configure your own setup.

### Who do I talk to? ###

* Zharif Zakaria

### Example docker-compose.yml ###
* compose file below will run mariadb instance with persistent data and persistant rrds.
```
version: '3'
services:
  db:
    restart: always
    image: mariadb
    volumes: 
      - "./dbdata:/var/lib/mysql"
    environment:
      MYSQL_ROOT_PASSWORD: verysecurepassword
  app:
    restart: always
    privileged: true
    image: zharifzkria/obs
    env_file:
      - env
    ports:
      - "8743:443/tcp"
      - "8744:514"
    volumes:
      - "./rrd:/opt/observium/rrd"
    depends_on:
      - db
```
### Environment variables to configure ###

|VARIABLE|DESCRIPTION|EXAMPLE|
|--------|-----------|-------|
|CONFENV |enable startup script to load enviroment vars|CONFEV=true|
|DBHOST  |MYSQL/MARIADB hostname or ip address|DBHOST=db|
|DBPASSWORD|MYSQL/MARIADB password for root|DBPASSWORD=password|
|USERNAME|Username for observium if not using LDAP|USERNAME=username|
|PASSWORD|Password for observium user above|PASSWORD=zaq123|
|LDAP|Enable LDAP (boolean)|LDAP=true|
|AUTHLDAPSERVER|LDAP Server hostname or ip address|AUTHLDAPSERVER="10.0.0.1"|
|AUTHLDAPPORT|LDAP Server port|AUTHLDAPPORT=389|
|AUTHLDAPUSER|LDAP DN to bind for auth|AUTHLDAPUSER="cn=obsauth,dc=org,dc=com"|
|AUTHLDAPPASS|LDAP DN to bind for auth's password|AUTHLDAPPASS="password"|
|AUTHLDAPOBJECT|LDAP object to indentify as valid user|AUTHLDAPOBJECT="posixAccount"|
|AUTHLDAPID|LDAP attribute to use as username|AUTHLDAPID="uid="|
|AUTHLDAPSUFFIX|LDAP filter for user|AUTHLDAPSUFFIX=",ou=Users,dc=org,dc=com"|
|AUTHLDAPGROUPADMIN|LDAP group assigned as admin|AUTHLDAPGROUPADMIN='cn=admins,ou=Groups,dc=org,dc=com'|
|AUTHLDAPGROUPREADALL|LDAP group assigned read all permission|AUTHLDAPGROUPREADALL='cn=readall,ou=Groups,dc=org,dc=com'|
|AUTHLDAPGROUPNORMAL|LDAP group assigned normal user|AUTHLDAPGROUPNORMAL='cn=normal,ou=Groups,dc=org,dc=com'|

