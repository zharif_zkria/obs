#!/bin/bash
DB=db
DBUSER=root
DBPASS=zaq123
ADMIN="z"
ADMINPASS="zaq123"
AUTHLDAP=true
AUTHDB="mysql"
LDAPSERVER="10.3.3.18"
LDAPPORT="389"
LDAPUSER="cn=Portainer Auth,dc=zhrif,dc=com"
LDAPPASS="zaq123"
LDAPOBJECT="posixAccount"
LDAPID="uid="
LDAPSUFFIX=",ou=Users,dc=zhrif,dc=com"
LDAPGROUPADMIN="cn=admins,ou=Groups,dc=zhrif,dc=com"
LDAPGROUPREADALL="cn=clients,ou=Groups,dc=zhrif,dc=com"
LDAPGROUPNORMAL="cn=readonly,ou=Groups,dc=zhrif,dc=com"

importvars (){
	DB=$DBHOST
	say info "DB : $DBHOST" 

	DBUSER=$DBUSERNAME
	say info "DBUSER : $DBUSERNAME"

	DBPASS=$DBPASSWORD
	say info "DBPASS : $DBPASSWORD"

	ADMIN=$USERNAME
	say info "ADMIN : $USERNAME"

	ADMINPASS=$PASSWORD
	say info "ADMINPASS : $PASSWORD"

	AUTHLDAP=$LDAP
	say info "AUTHLDAP : $LDAP"

	LDAPSERVER=$AUTHLDAPSERVER
	say info "LDAPSERVER : $AUTHLDAPSERVER"

	LDAPPORT=$AUTHLDAPPORT
	say info "LDAPPORT : $AUTHLDAPPORT"

	LDAPUSER=$AUTHLDAPUSER
	say info "LDAPUSER : $AUTHLDAPUSER"

	LDAPPASS=$AUTHLDAPPASS
	say info "LDAPPASS : $AUTHLDAPPASS"

	LDAPOBJECT=$AUTHLDAPOBJECT
	say info "LDAPOBJECT : $AUTHLDAPOBJECT"

	LDAPID=$AUTHLDAPID
	say info "LDAPID : $AUTHLDAPID"

	LDAPSUFFIX=$AUTHLDAPSUFFIX
	say info "LDAPSUFFIX : $AUTHLDAPSUFFIX"

	LDAPGROUPADMIN=$AUTHLDAPGROUPADMIN
	say info "LDAPGROUPADMIN : $AUTHLDAPGROUPADMIN"

	LDAPGROUPREADALL=$AUTHLDAPGROUPREADALL
	say info "LDAPGROUPREADALL : $AUTHLDAPGROUPREADALL"

	LDAPGROUPNORMAL=$AUTHLDAPGROUPNORMAL
	say info "LDAPGROUPNORMAL : $AUTHLDAPGROUPNORMAL"

	if [[ $AUTHLDAP ]]; then
		AUTHDB="ldap"
	fi
}

say () {
	if [ "$1" == "info" ];then
		echo -e "\e[34;1m[info]\e[0m $2"
	fi
	if [ "$1" == "error" ];then
		echo -e "\e[91;1m[error]\e[0m $2"
	fi
	if [ "$1" == "output" ];then
		echo -e "\e[32m[output]\e[0m $2"
	fi
}

help (){
	echo "usage: script [options] [args...]
  options:
    run 		default entrypoint run
    "
}

runsmoke (){
	/opt/observium/scripts/generate-smokeping.php > /etc/smokeping/config.d/Targets
	service smokeping restart
}

gen-rsysconf (){
	cat <<- EOF
	\$ModLoad imudp
	\$UDPServerRun 514
	\$PreserveFQDN on
EOF
}

gen-obsrsyconf (){
	cat <<- EOF
	#---------------------------------------------------------
#send remote logs to observium
template(name="observium"
         type="string"
         string="%fromhost%||%syslogfacility%||%syslogpriority%||%syslogseverity%||%syslogtag%||%\$year%-%\$month%-%\$day% %timereported:8:25%||%msg%||%programname%\n")
\$ModLoad omprog
# rsyslog Input Modules
input(type="imudp"
      port="514"
      ruleset="observium")

# rsyslog RuleSets
ruleset(name="observium") {
    action(type="omprog"
           binary="/opt/observium/syslog.php"
           template="observium")
}
*.* stop
#---------------------------------------------------------
EOF
}

gen-cron (){
	cat <<- EOF
	# Run a complete discovery of all devices once every 6 hours
	33 */6 * * * /opt/observium/discovery.php -h all >> /var/log/apache2/access.log

	# Run automated discovery of newly added devices every 5 minutes
	*/5 * * * * /opt/observium/discovery.php -h new >> /var/log/apache2/access.log

	# Run smokeping discovery of newly added devices every 5 minutes
	*/5 * * * * /entrypoint.sh runsmoke >> /var/log/apache2/access.log

	# Run multithreaded poller wrapper every 5 minutes
	*/5 * * * * /opt/observium/poller-wrapper.py 4 >> /var/log/apache2/access.log

	# Run housekeeping script daily for syslog, eventlog and alert log
	13 5 * * * /opt/observium/housekeeping.php -ysel >> /var/log/apache2/access.log

	# Run housekeeping script daily for rrds, ports, orphaned entries in the database and performance data
	47 4 * * * /opt/observium/housekeeping.php -yrptb >> /var/log/apache2/access.log
	EOF
}

gen-https (){
	cat <<- EOF
<IfModule mod_ssl.c>
	<VirtualHost _default_:443>
		ServerAdmin webmaster@localhost
		DocumentRoot /opt/observium/html
		SSLEngine on
        SSLCertificateFile /etc/apache2/ssl/apache.crt
        SSLCertificateKeyFile /etc/apache2/ssl/apache.key
    <FilesMatch \\.php\$>
    	SetHandler application/x-httpd-php
    </FilesMatch>
    <Directory />
    	Options FollowSymLinks
    	AllowOverride None
    </Directory>
    <Directory /opt/observium/html/>
    	DirectoryIndex index.php
    	Options Indexes FollowSymLinks MultiViews
    	AllowOverride All
    	Require all granted
    </Directory>
    ErrorLog  \${APACHE_LOG_DIR}/error.log
    LogLevel warn
    CustomLog  \${APACHE_LOG_DIR}/access.log combined
    ServerSignature On
	</VirtualHost>
</IfModule>
EOF
}

gen-config (){
	cat <<- EOF
<?php
\$config['db_extension'] = 'mysqli';
\$config['db_host']      = '$DB';
\$config['db_user']      = '$DBUSER';
\$config['db_pass']      = '$DBPASS';
\$config['db_name']      = 'observium';
\$config['snmp']['community'] = array("public");
\$config['auth_mechanism'] = "$AUTHDB";
\$config['smokeping']['dir'] = "/var/lib/smokeping/Observium/";
\$config['enable_syslog'] = 1;
\$config['auth_ldap_server'] = $LDAPSERVER;
\$config['auth_ldap_port'] = $LDAPPORT;
\$config['auth_ldap_suffix'] = $LDAPSUFFIX;
\$config['auth_ldap_prefix'] = $LDAPID;
\$config['auth_ldap_groups'][$LDAPGROUPADMIN]['level'] = 10;
\$config['auth_ldap_groups'][$LDAPGROUPNORMAL]['level'] = 1;
\$config['auth_ldap_groups'][$LDAPGROUPREADALL]['level'] = 5;
\$config['auth_ldap_binddn'] = $LDAPUSER;
\$config['auth_ldap_bindpw'] = $LDAPPASS;
\$config['auth_ldap_bindanonymous'] = FALSE;
\$config['auth_ldap_objectclass'] = $LDAPOBJECT;
EOF
}

makessl (){
	mkdir /etc/apache2/ssl
	openssl req -subj '/CN=domain.com/O=My Company Name LTD./C=US' -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.crt
	gen-https > /etc/apache2/sites-available/01-default-ssl.conf
	a2enmod ssl
	a2ensite 01-default-ssl.conf
}

makeuser (){
	/opt/observium/./adduser.php z zaq123 10
}

makedb (){
	while ! mysqladmin ping -h$DB -p$DBPASS --silent; do
		say info "DB not running.. sleeping for a sec"
		sleep 5
	done
	say info "DB is reachable initializing"
	mysql -h$DB -p$DBPASS -e "CREATE DATABASE observium DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;"
}

makereq (){
	mkdir /opt/observium/rrd
	mkdir /opt/observium/logs
	chown www-data:www-data /opt/observium/rrd
	phpenmod mcrypt
	a2dismod mpm_event
	a2enmod mpm_prefork
	a2enmod php7.0
	a2enmod rewrite
	chmod +s /usr/bin/*
}

addmib (){
	# modify /etc/snmp/snmp.conf
	sed -i -e 's@mibs :@mibdirs /opt/observium/mibs:/opt/observium/mibs/net-snmp@g' /etc/snmp/snmp.conf
}

logread (){
	say info "starting apache2"
	service apache2 start 2> /dev/null
	say info "starting cron"
	service cron start
	say info "starting smokeping"
	service smokeping start
	say info "reading of error logs"
	tail -f /var/log/apache2/error.log &
	say info "reading of access logs"
	touch /opt/observium/logs/observium.log
	tail -f /opt/observium/logs/observium.log &
	tail -f /var/log/apache2/access.log
}

init (){
	say info "Container not Initialized. Running initial setup"

	say info "===================================================="
	if [[ ! -z "$CONFENV" ]]; then
		say info "Custom environment vars detected"
		say info "Loading custom environment vars"
		importvars
	else
		say info "Loading default environment vars"
	fi
	say info "===================================================="

	say info "Initializing DB"
	makedb
	
	say info "Adding CRON"
	gen-cron > /observiumcron
	crontab /observiumcron
	rm /observiumcron

	say info "Starting CRON"
	service cron start
	
	say info "Generating config"
	gen-config > /opt/observium/config.php
	
	say info "Modifying SNMP config"
	addmib
	
	say info "Generating SSL and Apache2 config"
	makessl
	
	say info "Running required configs"
	makereq
	
	say info "Generating default user"
	makeuser
	
	say info "Generating rsyslog config"
	gen-rsysconf >> /etc/rsyslog.conf
	
	say info "Generating rsyslog config for observium"
	gen-obsrsyconf > /etc/rsyslog.d/30-observium.conf

	say info "Restarting rsyslog"
	service rsyslog restart

	say info "Schema Install"
	/opt/observium/./discovery.php -u

	say info "Mark as initialized"
	touch /opt/status && echo "1" > /opt/status
}

run (){
	say info "Running.."
	if [ -e "/opt/status" ]; then
		logread
	else
		init
		logread
	fi
}

say info "Starting Up"
say info "`date`"
if [ -z "$1" ];then
	help
	exit 0
else
	if [ $1 == "run" ] || [ $1 == "init" ] || [ $1 == "runsmoke" ] || [ $1 == "help" ] ; then
		$1
	else
		help
	fi
fi
